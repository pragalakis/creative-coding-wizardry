const random = require('canvas-sketch-util').random;

// Artwork function
module.exports = update => {
  let state = 0;
  document.body.onclick = e => {
    state++;
    random.setSeed(Math.random());
    update.render();
  };
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,100%,100%)';
    context.fillRect(0, 0, width, height);

    const margin = 1;
    const lh = 0.5; // line height
    const fontSize = 0.64;
    let text = 'TEXT ';
    let textIndex = -1;

    context.font = `bold ${fontSize}pt Roboto, Helvetica`;
    context.fillStyle = 'rgba(0,0,0,0.8)';

    if (state === 0) {
      context.font = `bold 4pt Roboto, Helvetica`;
      const textWidth = context.measureText(text).width;
      context.fillText(text, width / 2 - textWidth / 2, height / 2);
    } else if (state < 7) {
      const range = (min, max) => Math.random() * (max - min) + min;

      // draw text - a letter on each repetition
      for (let j = margin + fontSize; j < height - margin; j += fontSize + lh) {
        for (let i = margin / 1.5; i < width - margin; i += textLength) {
          if (state > 2) {
            text = 'SPRINKLED ';
          }

          textIndex = textIndex >= text.length - 1 ? 0 : textIndex + 1;
          let letter = text[textIndex];
          textLength = context.measureText(letter).width;

          if (state === 2) {
            if (Math.random() >= 0.5) continue;
          }

          if (state > 2) {
            // skip drawing
            if (range(0.2, 1.5) + Math.log(j) > 3.8) continue;
          }

          // draw letter
          context.fillText(letter, i, j);
        }
        // start again from the first letter
        textIndex = -1;
      }
    } else {
      // background
      context.fillStyle = 'hsl(0,0%,97%)';
      context.fillRect(0, 0, width, height);

      const fontSize = 0.5;
      context.fillStyle = 'hsl(0,0%,6%)';
      context.font = `${fontSize}pt Courier`;
      context.lineWidth = 0.05;
      const text = 'abcdefghijklmnopqrstuvwxyz'.split('');

      // draw
      for (let j = -1; j < height + fontSize; j += fontSize + 0.1) {
        for (let i = 0; i < width; i += fontSize) {
          // random letter
          const letter = text[Math.floor(Math.random() * text.length)];
          // noise
          const noise = random.noise2D(i * 0.05, j * 0.05);
          if (noise < 0.5) {
            context.fillText(letter, i, j);
          } else {
            context.fillText(
              letter,
              i + Math.random() * random.sign(),
              j + Math.random() * random.sign()
            );
          }
        }
      }
    }
  };
};
