// Artwork function
module.exports = update => {
  let text = Math.random();
  document.body.onclick = e => {
    text = Math.random();
    update.render();
  };
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,100%,100%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,0%)';
    context.fillStyle = 'rgb(0,0,0)';
    context.lineWidth = 0.5;
    context.font = '1.5pt Monospace, Consolas, monaco, courier';

    const textWidth = context.measureText(text).width;
    context.fillText(Math.random(), width / 2 - textWidth / 2, height / 2);
  };
};
