// Artwork function
module.exports = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,100%,100%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,0%)';
    context.strokeStyle = 'hsl(0,0%,0%)';
    context.strokeWidth = 'hsl(0,0%,0%)';
    context.font = `bold 1pt Roboto, Helvetica`;
    context.lineWidth = 0.3;

    const s = 20;
    context.beginPath();
    context.moveTo(width / 2 - s / 2, 8);
    context.bezierCurveTo(
      width / 2 - s / 2,
      8 + s / 2,
      width / 2 + s / 2,
      8 + s / 2,
      width / 2 + s / 2,
      8
    );
    context.stroke();

    context.fillText('Start Position', width / 2 - s / 2 - 3.5, 7);
    context.fillText('End Position', width / 2 + s / 2 - 4, 7);
    context.fillText('Control Position', width / 2 - 4, 8 + s / 2);
    context.beginPath();
    context.arc(width / 2 + s / 2, 8, 0.5, 0, Math.PI * 2, true);
    context.fill();
    context.beginPath();
    context.arc(width / 2 - s / 2, 8, 0.5, 0, Math.PI * 2, true);
    context.fill();

    context.beginPath();
    context.moveTo(width / 2 - s / 2, height / 2 + 10);
    context.quadraticCurveTo(
      width / 2 + s / 2,
      height / 2 + 10 - s / 2,
      width / 2 + s / 2,
      height / 2 + 10
    );
    context.stroke();

    context.fillText('Start Position', 2, height / 2 + 12);
    context.fillText('End Position', width / 2 + 5, height / 2 + 12);
    context.fillText('Control Position', width / 2 + 2, height / 2 + 4);
    context.beginPath();
    context.arc(width / 2 - s / 2, height / 2 + 10, 0.5, 0, Math.PI * 2, true);
    context.fill();
    context.beginPath();
    context.arc(width / 2 + s / 2, height / 2 + 10, 0.5, 0, Math.PI * 2, true);
    context.fill();
  };
};
