// Artwork function
module.exports = () => {
  return ({ context, width, height }) => {
    // equilateral triangle
    function triangle(size, x, y) {
      context.beginPath();
      context.moveTo(size * Math.cos(0) + x, size * Math.sin(0) + y);
      context.lineTo(
        size * Math.cos((1 / 3) * 2 * Math.PI) + x,
        size * Math.sin((1 / 3) * 2 * Math.PI) + y
      );
      context.lineTo(
        size * Math.cos((2 / 3) * 2 * Math.PI) + x,
        size * Math.sin((2 / 3) * 2 * Math.PI) + y
      );
      context.lineTo(size * Math.cos(0) + x, size * Math.sin(0) + y);
      context.fill();
    }

    // background
    context.fillStyle = 'hsl(0,100%,100%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,0%)';
    context.lineWidth = 0.5;

    const margin = 1;
    const rad = (width - 10 - margin * (10 / 2 - 1)) / 10;
    for (let i = 6.3; i <= width - 4; i += 2 * rad + margin) {
      for (let j = 4; j < height / 3; j += 2 * rad + margin) {
        context.fillStyle = `rgb(${Math.random() * 255},${Math.random() *
          255},${Math.random() * 255})`;
        context.beginPath();
        context.arc(i, j, rad, 0, Math.PI * 2, true);
        context.fill();
      }
    }

    for (let i = 6.3; i <= width - 5; i += rad * 2 + margin) {
      for (
        let j = height / 3 + rad + margin;
        j <= height - height / 3;
        j += rad * 2 + margin
      ) {
        context.fillStyle = `rgb(${Math.random() * 255},${Math.random() *
          255},${Math.random() * 255})`;
        context.fillRect(i - rad, j - rad, rad * 2, rad * 2);
      }
    }

    for (let i = 6.3; i <= width - 5; i += rad * 2 + margin) {
      for (
        let j = height - height / 3 + rad;
        j < height - margin;
        j += rad * 2 + margin
      ) {
        context.fillStyle = `rgb(${Math.random() * 255},${Math.random() *
          255},${Math.random() * 255})`;
        triangle(1.3 * rad, i, j);
      }
    }
  };
};
