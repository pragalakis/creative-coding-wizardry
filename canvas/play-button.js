// Artwork function
module.exports = canvas => {
  const context = canvas.getContext('2d');
  const width = canvas.width;
  const height = canvas.height;

  context.strokeStyle = 'black';
  context.lineWidth = 0.5;

  context.beginPath();
  context.moveTo(0, 0);
  context.lineTo(0, height);
  context.lineTo(width, height / 2);
  context.closePath();
  context.clip();

  let n = 40;
  while (n > 0) {
    context.beginPath();
    context.moveTo(Math.random() * width, 0);
    context.lineTo(Math.random() * width, height);
    context.stroke();

    context.beginPath();
    context.moveTo(0, Math.random() * height);
    context.lineTo(width, Math.random() * height);
    context.stroke();
    n--;
  }
};
