// Artwork function
module.exports = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,100%,100%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,0%)';
    context.strokeStyle = 'hsl(0,0%,0%)';
    context.lineWidth = 0.3;

    context.font = `bold 1.5pt Roboto, Helvetica`;
    context.fillText('Start position', 6, 3);
    context.fillText('End position', width - 18, height - 2);

    context.beginPath();
    context.moveTo(4, 2);
    context.lineTo(width - 4, height - 2);
    context.stroke();

    context.beginPath();
    context.arc(width - 4, height - 2, 0.5, 0, Math.PI * 2, true);
    context.fill();
    context.beginPath();
    context.arc(4, 2, 0.5, 0, Math.PI * 2, true);
    context.fill();
  };
};
