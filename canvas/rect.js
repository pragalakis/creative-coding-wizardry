// Artwork function
module.exports = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,100%,100%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,0%)';
    context.fillStyle = 'hsl(0,0%,0%)';
    context.lineWidth = 0.3;

    context.font = `bold 1pt Roboto, Helvetica`;
    context.fillText('Start', 7, 2.5);
    context.fillText('Position', 7, 4);
    context.fillText('Start', 4, height / 2 + 2.5);
    context.fillText('Position', 4, height / 2 + 4);
    context.fillText('Width', 13, height / 2 - 4);
    context.fillText('Height', width - 9, height / 2 - 10);
    context.fillText('Width', 13, height - 6);
    context.fillText('Height', width - 6, height - 11);

    context.beginPath();
    context.fillStyle = 'rgb(255,0,0)';
    context.fillRect(width / 2 - 5, 5, 10, 10);
    context.strokeRect(width / 2 - 5, 5, 10, 10);
    context.fillStyle = 'rgb(0,0,0)';
    context.beginPath();
    context.arc(width / 2 - 5, 5, 0.5, 0, Math.PI * 2, true);
    context.fill();

    context.fillStyle = 'rgb(255,255,0)';
    context.fillRect(width / 2 - 8, height / 2 + 5, 16, 8);
    context.strokeRect(width / 2 - 8, height / 2 + 5, 16, 8);
    context.fillStyle = 'rgb(0,0,0)';
    context.beginPath();
    context.arc(width / 2 - 8, height / 2 + 5, 0.5, 0, Math.PI * 2, true);
    context.fill();
  };
};
