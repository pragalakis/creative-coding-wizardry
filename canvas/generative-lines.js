// Artwork function
module.exports = update => {
  let state = 0;
  document.body.onclick = e => {
    state++;
    update.render();
  };
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,100%,100%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,0%)';
    context.fillStyle = 'rgb(0,0,0)';
    context.lineWidth = 0.5;

    if (state === 0) {
      context.beginPath();
      context.moveTo(0, 0);
      context.lineTo(width, height);
      context.stroke();
    } else if (state === 1) {
      context.beginPath();
      context.moveTo(width, 0);
      context.lineTo(0, height);
      context.stroke();
    } else if (state === 2) {
      context.beginPath();
      context.moveTo(0, 0);
      context.lineTo(width, height);
      context.stroke();
      context.beginPath();
      context.moveTo(width, 0);
      context.lineTo(0, height);
      context.stroke();
    } else {
      const size = 40 / state;
      context.lineWidth = 0.2;
      for (let i = 0; i <= width; i += size) {
        for (let j = 0; j <= height; j += size) {
          context.beginPath();
          if (Math.random() >= 0.5) {
            context.moveTo(i, j);
            context.lineTo(i + size, j + size);
          } else {
            context.moveTo(i + size, j);
            context.lineTo(i, j + size);
          }
          context.stroke();
        }
      }
    }
  };
};
