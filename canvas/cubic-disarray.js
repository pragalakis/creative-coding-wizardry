// Artwork function
module.exports = update => {
  let state = 0;
  document.body.onclick = e => {
    state++;
    update.render();
  };
  return ({ context, width, height }) => {
    // Off-white background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.translate(-0.2, -0.5);
    context.lineWidth = 0.2;

    let size = 2;
    if (state >= 10) size = 1.5;
    let rotation = Math.PI / 180;
    let rnd = () => (Math.random() < 0.5 ? -Math.random() : Math.random());

    // draw squares
    for (let i = size; i <= width - size; i += size) {
      for (let j = size; j <= height - size; j += size) {
        context.save();
        let rotate;
        if (state === 1) rotate = rnd();
        if (state > 1 && state < 10) rotate = j * rotation * rnd();
        if (state >= 10) {
          let distanceToCenter;
          if (state < 15) {
            distanceToCenter = Math.abs(j - height / 2);
          } else {
            distanceToCenter = height / 2 - Math.abs(j - height / 2);
          }
          rotate = distanceToCenter * rotation * rnd();
        }
        context.translate(i + size / 2, j + size / 2);
        context.rotate(rotate);
        if (state <= 1) context.strokeRect(-size / 2, -size / 2, size, size);
        if (state > 1 && state < 10)
          context.strokeRect(
            -size / 2 + rotate,
            -size / 2 + rotate,
            size,
            size
          );

        if (state >= 10)
          context.strokeRect(
            -size / 2 + rotate,
            -size / 2 + rotate,
            size,
            size
          );
        context.restore();
      }
    }
  };
};
