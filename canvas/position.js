// Artwork function
module.exports = update => {
  document.body.onclick = e => {
    update.render();
  };

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,100%,100%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,0%)';
    context.strokeStyle = 'hsl(0,0%,0%)';
    context.lineWidth = 0.3;

    context.font = `bold 1pt Roboto, Helvetica`;
    const x = [9, 12, 15, 18, 21];
    const y = [
      height / 2 + 5.5,
      height / 2 + 2.5,
      height / 2 - 0.5,
      height / 2 - 3.5,
      height / 2 - 6.5
    ];

    const baseX = 5.5;
    const baseY = height / 2 + 8.5;
    const rad = 0.3;

    // numbers
    context.fillText('5', baseX, y[4]);
    context.fillText('4', baseX, y[3]);
    context.fillText('3', baseX, y[2]);
    context.fillText('2', baseX, y[1]);
    context.fillText('1', baseX, y[0]);
    context.fillText('0', baseX, baseY);
    context.fillText('1', x[0], baseY);
    context.fillText('2', x[1], baseY);
    context.fillText('3', x[2], baseY);
    context.fillText('4', x[3], baseY);
    context.fillText('5', x[4], baseY);

    // axis
    context.beginPath();
    context.moveTo(7, height / 2 - 8);
    context.lineTo(7, height / 2 + 7);
    context.lineTo(22, height / 2 + 7);
    context.stroke();
    context.font = `bold 2pt Roboto, Helvetica`;
    context.fillText('y', baseX + 1, height / 2 - 9);
    context.fillText('x', 23, baseY - 1);

    // random points
    context.font = `bold 1pt Roboto, Helvetica`;
    context.lineWidth = 0.2;
    const randomX = x[Math.floor(Math.random() * x.length)];
    const randomY = y[Math.floor(Math.random() * y.length)];
    context.fillText(
      `(${x.indexOf(randomX) + 1},${y.indexOf(randomY) + 1})`,
      randomX + 0.5,
      randomY - 0.5
    );
    context.beginPath();
    context.moveTo(randomX, baseY - 1.5);
    context.lineTo(randomX, randomY);
    context.lineTo(baseX + 1.5, randomY);
    context.stroke();
    context.beginPath();
    context.arc(randomX, randomY, rad, 0, Math.PI * 2, true);
    context.fill();
  };
};
