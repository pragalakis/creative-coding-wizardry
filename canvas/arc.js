// Artwork function
module.exports = () => {
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,100%,100%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,0%)';
    context.lineWidth = 0.3;

    context.fillStyle = 'rgb(0,255,0)';
    context.beginPath();
    context.arc(width / 2, height / 2, 10, 0, Math.PI * 2);
    context.fill();
    context.stroke();

    context.fillStyle = 'rgb(0,0,0)';
    context.beginPath();
    context.arc(width / 2, height / 2, 0.5, 0, Math.PI * 2);
    context.fill();

    context.font = `bold 1pt Roboto, Helvetica`;
    context.fillText('Center', width / 2 - 2, height / 2 - 3);
    context.fillText('Position', width / 2 - 2, height / 2 - 1.5);

    context.lineWidth = 0.2;
    context.beginPath();
    context.moveTo(width / 2, height / 2);
    context.lineTo(width / 2 + 10, height / 2);
    context.stroke();

    context.fillText('Radius', width / 2 + 3.5, height / 2 + 1.5);
  };
};
