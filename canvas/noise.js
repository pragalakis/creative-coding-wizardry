const { lerp } = require('canvas-sketch-util/math');
const random = require('canvas-sketch-util/random');
const palettes = require('nice-color-palettes');

// Artwork function
module.exports = update => {
  let state = 0;
  document.body.onclick = e => {
    state++;
    update.render();
  };
  return ({ context, width, height }) => {
    // background color
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);
    context.lineWidth = 0.2;

    if (state === 0) {
      for (let i = 0; i < width; i++) {
        for (let j = 1; j < height; j++) {
          context.beginPath();
          context.moveTo(i, j);
          context.lineTo(i + 0.8, j);
          context.stroke();
        }
      }
    }

    if (state === 1) {
      let step = 1;
      for (let i = 0; i < width; i += step) {
        for (let j = 1; j < height; j += step) {
          const noise = random.noise2D(i * 0.05, j * 0.05);
          context.beginPath();
          context.moveTo(i + noise - step / 2, j + noise);
          context.lineTo(i + noise + step / 2, j + noise);
          context.stroke();
        }
      }
    }

    if (state === 2) {
      let step = 1;
      for (let i = 0; i < width; i += step) {
        for (let j = 1; j < height; j += step) {
          const noise = random.noise2D(i * 0.05, j * 0.05);
          context.save();
          context.beginPath();
          context.translate(i, j);
          context.rotate(noise);
          context.translate(-i, -j);
          context.moveTo(i - step / 2, j);
          context.lineTo(i + step / 2, j);
          context.stroke();
          context.restore();
        }
      }
    }

    if (state > 2) {
      // If you want to see more of this, check Matt Desl's work
      const symbols = ['▬', '‑', '‑', '-', '-', '‒', '—', '‒', '—', '▲', '●'];
      const palette = palettes[Math.floor(Math.random() * palettes.length)];

      const points = function() {
        const array = [];
        const N = 55;
        for (let i = 2; i < N; i++) {
          for (let j = 2; j < N; j++) {
            let x = i / (N - 1);
            let y = j / (N - 1);
            let radius = 0.03 + Math.abs(random.noise2D(x, y, 2) * 0.05);

            array.push([x, y, radius]);
          }
        }
        return array;
      };

      // remove points randomly
      const data = points().filter(() => Math.random() > 0.6);
      const margin = 0;

      data.forEach(v => {
        const rotation = random.noise2D(v[0], v[1]);
        const color = palette[Math.floor(Math.random() * palette.length)];
        const character = symbols[Math.floor(Math.random() * symbols.length)];
        const fontSize = v[2] * width;

        const x = lerp(margin, width - 2 * margin, v[0]);
        const y = lerp(margin, height - 1.5 * margin, v[1]);

        context.save();
        context.fillStyle = color;
        context.font = `${fontSize}px Arial`;
        context.translate(x, y);
        context.rotate(rotation);
        context.fillText(character, 0, 0);
        context.restore();
      });
    }
  };
};
