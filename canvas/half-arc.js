const palettes = require('nice-color-palettes');

// Artwork function
module.exports = update => {
  let state = 0;
  document.body.onclick = e => {
    state++;
    update.render();
  };
  return ({ context, width, height }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);
    if (state === 0) {
      context.strokeStyle = 'black';
      context.strokeWidth = 1;
      context.beginPath();
      context.arc(width / 2, height / 2, 8, 0, Math.PI * 2, true);
      context.stroke();
    } else if (state === 1) {
      context.fillStyle = 'black';
      context.beginPath();
      context.arc(width / 2, height / 2, 8, Math.PI, Math.PI * 2, true);
      context.fill();
    } else if (state === 2) {
      context.strokeStyle = 'black';
      context.lineWidth = 0.2;
      for (let i = 2.2; i < width; i += 2.5) {
        for (let j = 2.2; j < height; j += 2.5) {
          context.beginPath();
          context.arc(i, j, 1, 0, Math.PI * 2, true);
          context.stroke();
        }
      }
    } else {
      let size = 1;
      if (state > 3) {
        size = 0.3 + Math.random() * 3;
      }
      let colors = ['black', 'white'];

      function circles(i, j, colors) {
        let rnd = Math.random();
        let angle = 1.57;
        let color = Math.floor(Math.random() * colors.length);

        // background color
        context.fillStyle = `${colors[color]}`;
        context.fillRect(i, j, size, size);

        // circle
        context.beginPath();
        context.save();
        if (rnd < 0.25) {
          context.translate(0, size / 2);
          context.arc(i, j, size * 0.5, angle, angle + Math.PI, true);
        } else if (rnd >= 0.25 && rnd < 0.5) {
          context.translate(size / 2, 0);
          context.arc(i, j, size * 0.5, 2 * angle, 2 * angle + Math.PI, true);
        } else if (rnd >= 0.5 && rnd < 0.75) {
          context.translate(size, size / 2);
          context.arc(i, j, size * 0.5, 3 * angle, 3 * angle + Math.PI, true);
        } else {
          context.translate(size / 2, size);
          context.arc(i, j, size * 0.5, 0, Math.PI, true);
        }

        context.restore();

        // reverse color
        color = color == 1 ? 0 : 1;
        context.fillStyle = `${colors[color]}`;
        context.fill();
      }

      context.fillStyle = 'black';
      context.fillRect(0, 0, width, height);
      context.translate(-0.13, 0);

      for (let j = 0; j < height; j += size) {
        for (let i = 0; i <= width; i += size) {
          let colors = ['black', 'white'];
          if (state > 3) {
            const palette =
              palettes[Math.floor(Math.random() * palettes.length)];
            colors = [
              palette[Math.floor(Math.random() * palette.length)],
              palette[Math.floor(Math.random() * palette.length)]
            ];
          }
          circles(i, j, colors);
        }
      }
    }
  };
};
