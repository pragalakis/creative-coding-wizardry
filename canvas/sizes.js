// Artwork function
module.exports = update => {
  document.body.onclick = e => {
    update.render();
  };

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,100%,100%)';
    context.fillRect(0, 0, width, height);

    context.fillStyle = 'hsl(0,0%,0%)';
    context.strokeStyle = 'hsl(0,0%,0%)';
    context.lineWidth = 0.3;

    context.font = `bold 1.5pt Roboto, Helvetica`;
    context.fillText('400 px', width / 2 - 3, 9);
    context.fillText('10 cm', width / 2 - 3, 19);
    context.fillText('6.2 inch', width / 2 - 3, 29);

    context.beginPath();
    context.moveTo(0, 10);
    context.lineTo(width, 10);
    context.stroke();

    context.beginPath();
    context.moveTo(width / 2 - 5, 20);
    context.lineTo(width / 2 + 5, 20);
    context.stroke();

    context.beginPath();
    context.moveTo(width / 2 - 8, 30);
    context.lineTo(width / 2 + 8, 30);
    context.stroke();
  };
};
