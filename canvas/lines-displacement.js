// Artwork function
module.exports = update => {
  let state = 0;
  document.body.onclick = e => {
    state++;
    update.render();
  };
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,100%,100%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,0%)';

    if (state === 0) {
      context.lineWidth = 1;
      context.beginPath();
      context.moveTo(0, height / 2);
      context.lineTo(width, height / 2);
      context.stroke();
    } else if (state === 1) {
      for (let j = 0; j < height; j += 2) {
        context.lineWidth = 0.3;
        context.beginPath();
        context.moveTo(0, j);
        context.lineTo(width, j);
        context.stroke();
      }
    } else {
      context.lineWidth = 0.2;
      if (state > 2) {
        context.fillStyle = 'hsl(0,0%,0%)';
        context.fillRect(0, 0, width, height);
        context.strokeStyle = 'hsl(0,100%,100%)';
      }
      for (let j = 3; j < height - 1; j++) {
        context.beginPath();
        context.moveTo(0, j);
        for (let i = 0; i < width; i++) {
          let random = Math.random();
          if (state > 2) {
            context.lineWidth = 0.1;
            let d = Math.abs(i - width / 2);
            let variance = Math.max(0, width / 2 - 2 - d);
            random = ((Math.random() * variance) / 7) * -1;
          }
          context.lineTo(i, j + random);
        }
        context.fill();
        context.stroke();
      }
    }
  };
};
