// Artwork function
module.exports = update => {
  let state = 0;
  document.body.onclick = e => {
    state++;
    update.render();
  };

  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0,0%,97%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0,0%,5%)';
    context.lineWidth = 0.2;

    if (state === 0) {
      context.lineWidth = 1;
      context.strokeRect(width / 2 - 5, height / 2 - 5, 10, 10);
    } else if (state === 1) {
      for (let i = 1; i < width; i += 2) {
        for (let j = 1; j < height; j += 2) {
          context.strokeRect(i, j, 2, 2);
        }
      }
    } else {
      const sign = () => (Math.random() >= 0.5 ? -1 : 1);
      const sizes = [1, 1.3, 1.8, 2, 2.3, 3];
      const size = sizes[Math.floor(Math.random() * sizes.length)];
      const lines = [];
      // calculate grid points for each line
      for (let j = size / 2.8; j < height; j += size) {
        const line = [];
        const noise = j * 0.02;
        for (let i = size / 2.5; i < width; i += size) {
          const x = i + (noise * (sign() * Math.random())) / 2;
          const y = j + (noise * (sign() * Math.random())) / 2;
          line.push([x, y]);
        }
        lines.push(line);
      }

      let flag = true;
      // draw grid
      lines.forEach((line, i) => {
        line.forEach((points, j) => {
          if (flag) {
            if (j < line.length - 1 && i < lines.length - 1) {
              context.beginPath();
              context.moveTo(lines[i + 1][j + 1][0], lines[i + 1][j + 1][1]);
              context.lineTo(lines[i + 1][j][0], lines[i + 1][j][1]);
              context.lineTo(points[0], points[1]);
              context.lineTo(line[j + 1][0], line[j + 1][1]);
              context.closePath();
              context.stroke();
            }
          } else {
            if (j <= line.length - 1 && i < lines.length - 1) {
              context.beginPath();
              context.moveTo(points[0], points[1]);
              context.lineTo(lines[i + 1][j][0], lines[i + 1][j][1]);
              context.stroke();
            }
          }
        });
        flag = !flag;
      });
    }
  };
};
