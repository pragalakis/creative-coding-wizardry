const load = require('load-asset');

module.exports = async ({ update }) => {
  const image = await load('./assets/mona-lisa.jpg');
  const options = {
    dimensions: [image.width, image.height],
    pixelsPerInch: 72,
    units: 'px'
  };
  let state = 0;
  update(options);

  document.body.onclick = e => {
    if (state < 11) {
      state++;
      update(options);
    }
  };

  return ({ context, width, height }) => {
    context.drawImage(image, 0, 0, width, height);

    const pixels = context.getImageData(0, 0, width, height);
    const data = pixels.data;
    const bytes = 4;
    let step;
    if (state === 5) {
      step = 40;
    } else if (state === 11) {
      step = 1;
    } else {
      step = 10 * state;
    }

    if (state === 0) {
      context.drawImage(image, 0, 0, width, height);
    } else if (state === 1) {
      context.drawImage(image, 0, 0, width, height);
      const size = 30;
      for (let j = 0; j < width; j += size) {
        for (let i = 0; i < height; i += size) {
          context.strokeRect(j, i, size, size);
        }
      }
    } else if ((state > 1 && state <= 5) || state === 11) {
      context.clearRect(0, 0, width, height);

      let offset = 0;

      for (let j = 0; j < width; j += step) {
        for (let i = 0; i < height; i += step) {
          let colorIndex = i * (width * bytes) + j * bytes;

          let r = data[colorIndex];
          let g = data[colorIndex + 1];
          let b = data[colorIndex + 2];
          let a = data[colorIndex + 3];

          if (state === 11) {
            data[colorIndex] = 255 - r;
            data[colorIndex + 1] = 255 - g;
            data[colorIndex + 2] = 255 - b;
          }

          context.fillStyle = `rgba(${r},${g},${b},${a})`;
          if (state === 5) {
            const rad = step / 2.6 <= 0 ? 0.1 : step / 2.6;
            context.beginPath();
            context.arc(j, i + 30, rad, 0, Math.PI * 2, true);
            context.fill();
          } else if (state < 6) {
            context.fillRect(j, i, step - offset, step - offset);
          }
        }
      }
      if (state === 11) context.putImageData(pixels, 0, 0);
    } else if (state >= 6 && state < 10) {
      context.clearRect(0, 0, width, height);

      context.fillStyle = 'hsl(0,0%,0%)';
      context.fillRect(0, 0, width, height);

      const bytes = 4;
      const size = 38;
      for (let j = size / 2; j < height; j += size) {
        for (let i = size / 2; i < width; i += size) {
          const colorIndex = j * (width * bytes) + i * bytes;
          let r = data[colorIndex];
          let g = data[colorIndex + 1];
          let b = data[colorIndex + 2];
          let a = data[colorIndex + 3];
          context.fillStyle = `rgb(${r},${g},${b})`;

          let rad = size / 4;
          const c = r;
          if (c < 80) {
            rad += 2.5 * Math.random();
          } else if (c < 160) {
            rad += 7.5 * Math.random();
          } else if (c < 240) {
            rad -= 5 * Math.random();
          } else {
            rad -= 10 * Math.random();
          }

          context.beginPath();
          context.arc(i, j, rad, 0, Math.PI * 2, true);
          context.fill();
        }
      }
    } else if (state === 10) {
      context.drawImage(image, 0, 0, width, height);

      const pixels = context.getImageData(0, 0, width, height);
      const data = pixels.data;

      const stepX = width / 13;
      const stepY = height / 20;
      const imgData = [];
      const points = [];

      function randomTile(arr, i) {
        let index = Math.floor(Math.random() * arr.length);
        return index != i && arr[index] != 0
          ? arr.splice(index, 1, 0)[0]
          : randomTile(arr, i);
      }

      for (let j = 0; j < height; j += stepY) {
        for (let i = 0; i < width; i += stepX) {
          imgData.push(context.getImageData(i, j, stepX, stepY));
          points.push([i, j]);
        }
      }

      points.forEach((tile, i) => {
        context.putImageData(randomTile(imgData, i), tile[0], tile[1]);
      });
    }
  };
};
