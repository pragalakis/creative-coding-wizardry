let random = require('canvas-sketch-util/random');

// Artwork function
module.exports = update => {
  let state = 0;
  document.body.onclick = e => {
    state++;
    update.render();
  };
  return ({ context, width, height }) => {
    // background
    context.fillStyle = 'hsl(0, 0%, 98%)';
    context.fillRect(0, 0, width, height);

    context.strokeStyle = 'hsl(0, 0%, 8%)';
    context.lineWidth = 0.1;

    if (state === 0) {
      context.lineWidth = 0.3;
      context.beginPath();
      context.arc(width / 2, height / 2, 13, 0, Math.PI * 2, true);
      context.stroke();
    }

    if (state === 1) {
      context.lineWidth = 0.2;
      let rad = 13;
      while (rad > 0) {
        context.beginPath();
        context.arc(width / 2, height / 2, rad, 0, Math.PI * 2, true);
        context.stroke();
        rad -= 0.8;
      }
    }

    if (state === 2) {
      function points(rad) {
        const N = 127;
        const data = Array(N)
          .fill()
          .map((v, i) => {
            const point = 0.05 * i;
            const noise = random.noise1D(point * 1.5) * rad * 0.05;
            const phi = point + Math.PI * 2;

            const x = rad * Math.cos(phi) + noise;
            const y = rad * Math.sin(phi) + noise;
            return [x, y];
          })
          .slice(0, N - 3);

        context.beginPath();
        data.forEach(v => {
          context.lineTo(width / 2 + v[0], height / 2 + v[1]);
        });

        context.closePath();
        context.stroke();
      }

      let rad = 1;
      context.lineWidth = 0.2;
      while (rad < 13) {
        points(rad);
        rad += 1;
      }
    }

    if (state > 2) {
      function points(rad) {
        const N = 127;
        const data = Array(N)
          .fill()
          .map((v, i) => {
            random = random.createRandom();
            const point = 0.05 * i;
            const noise = random.noise1D(point * 2) * rad * 0.025;
            const phi = point + Math.PI * 2;

            const x = rad * Math.cos(phi) + noise;
            const y = rad * Math.sin(phi) + noise;

            return [x, y];
          })
          .slice(0, N - 3);

        context.beginPath();
        data.forEach(v => {
          context.lineTo(width / 2 + v[0], height / 2 + v[1]);
        });

        context.closePath();
        context.stroke();
      }

      let rad = 0.5;

      while (rad < 13) {
        points(rad);
        rad += 0.4;
      }
    }
  };
};
