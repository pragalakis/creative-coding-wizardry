const html = require('choo/html');
const css = require('sheetify');

const showcase = () => {
  let isRunning = false;
  let scrollToBottom;
  let s = 0;
  const i = 20;
  function autoScroll() {
    scrollToBottom = setInterval(() => {
      s += i;
      scroll(0, s);
    }, 100);
  }

  function toggleAutoScroll() {
    if (!isRunning) {
      isRunning = true;
      autoScroll();
    } else {
      clearInterval(scrollToBottom);
      isRunning = false;
    }
  }

  return html`
    <body class=${prefix} onclick=${toggleAutoScroll}>
      <div>
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-drag.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/pixel-image.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-image.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-negative.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-img.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-size-img.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-img-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-dice.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-dice-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-censored.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-lines-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-lines-4.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-ception.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/pseudofractals.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/pseudofractals-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/pseudofractals-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/spiral.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rings.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/blue-red.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/flower.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/shell.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rotate.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/density.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/swirl.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arcs-border.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/phase.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/golden-flower.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/triangle-angle-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/triangle-angle.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-angle.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-angle-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/checkered-cubes.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/checkered-cubes-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/wave-rects.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-recursion.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-recursion-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/half-arc-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-repetition.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-rect.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/clock.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/clock-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/clock-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/clock-4.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/clock-5.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/clock-6.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/clock-7.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/clock-8.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-recursion-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circles-fold.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circles-fold-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/checkered-lenses.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/checkered-planes.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/checkered-planes-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/alternate.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/twist.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/twist-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/twist-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/wave-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-opacity.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circles-cos.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/polka.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/polka-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/interrupted-circle.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/square-wave.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/interrupted-circle-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/interrupted-circle-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-depth.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-center.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/ellipse-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/square-numbers.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/hexagon-grid.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/crosswords-text.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/grid-circle.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-grid-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-ladder.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-step.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-checkered.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-wave.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-circle-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-circle-rotation-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-rotation-arrow.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/no-events-here.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-sun.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-tower.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circles-pattern.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-creative-coding-athens.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/chevron-pattern.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-clipping.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/squares-text.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-cards-lifo.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-opacity.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-ath.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/net.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/net-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-cosine.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/enalax.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/roger-vilder-circle-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/square-packing.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/square-packing-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/depth.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/cosine-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/depth-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/nyan-trail.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/joy-division.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/hirst.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/hirst-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/mondrian.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/bloabs.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rect-arc-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/glitch.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/reverse-triangle.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-noise-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-glitch.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-circle-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/ascii-img.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-text-noise.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-text-size.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-tiles-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-tiles-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-tiles-4.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-tiles-5.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-tiles-6.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-tiles-8.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/glitch-img.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-shuffle.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-dots.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-lines-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-curve.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/img-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/interactions.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/hypnotic-squares.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/square-grid-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/tiled-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circles.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/fade.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/points.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/intersections.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/cubic-disarray.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rect-arc.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/shapes.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/half-arc.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/crosswords.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/xndash.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arcndash.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/deviation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/wave.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/waves.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rect-noise.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/points-border.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/pyramids.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/cubes.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/hor-ver.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/points-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/beam.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/sine.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rectception.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arcception.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/million-points.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/line-angles.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/square-noise.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circles-noise.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arcs-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arcs-lines-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arcs-lines-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arcs-lines-4.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arcs-random.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/falling-blocks.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/torn-rect.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/shapes-points.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rect-points-shadow.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/sphere-points.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-packing.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-packing-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rect-packing.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/patchwork.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/patchwork-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/patchwork-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/patchwork-4.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/city.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/characters.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/hor-ver-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-sprinkled.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/chaos-sierpinski.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/chaos-game.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/chaos-game-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/chaos-game-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/square-distance.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/pattern-circles.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/pattern-circles-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/pattern-circles-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/sinewave-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/textile.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/shades-of-red.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/shades-of-blue.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/shades-of-yellow.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/square-circle-triangle.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arc-pattern.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arcs-corners.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circles-flip.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/moonlight.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/the-big-6.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/introvert.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/nope.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/not-a-poster.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/gradient-discs.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/line-wave.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/bloabs-stacked.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/bloabs-stacked-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/bloabs-stacked-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/bloabs-stacked-4.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/square-sub-square.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circuit.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-random-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circles-tremble.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/numbers.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/404.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/glitch-circles.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/glitch-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/glitch-spiral.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/stripes-circle-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/tiles-lines-diff.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-deviation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-noise.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-noise-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/triangles-displacement.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/grid-displacement.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/grid-displacement-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/un-deux-trois.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/un-deux-trois-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/hexagon-noise.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/wave-noise.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arcs-colors.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/tiles-colors.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/ellipses-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/tree-grid.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/noise-map.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/noise-map-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-grid.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/curves.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rect-lines-coords.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-chaos-control.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-chaos-control-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/window-rain.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines-cross.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-disposition.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-grid.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/grid-div.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/grid-div-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/grid-circle-points.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-grid-random-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/checkered-pattern-generation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/bezier-circle.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/bezier-circles.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/quadratic-circles.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/points-on-squares.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-random-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/points-on-square.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-points-connect.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-points-connect-random.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/bezier-circle-points.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-bg-random-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines-diagonal.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines-diagonal-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/shapes-random.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/shapes-random-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/shapes-random-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/noise-lines-grid.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/quadrant-random.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/quadrant-random-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/arcs-lines-random.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/bloabs-line-width.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/space-division-recursion.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/text-hex.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/cubes-diagonal.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines-angle-calc.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/corner.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-points-noise.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-points-noise-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/faces.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/squares-lines-rotation-opacity.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines-opacity.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/grid-subgrid.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/square-polygon-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/polygons-lines.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/polygons-lines-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/polygons-lines-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-vera-molnar.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-vera-molnar-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-vera-molnar-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-vera-molnar-4.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines-vera-molnar.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines-vera-molnar-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines-vera-molnar-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/squares-vera-molnar.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/squares-vera-molnar-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-random-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/half-circle-rotation.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/square-rects.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/rects-random-centered.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/line-walk.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/christmas-tree.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/identicons-random.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/circle-trail.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/abstract.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/airflow.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/cosine-lines-random.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/aurora.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/aurora-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/aurora-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/aurora-4.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/lines-pencil.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/flow.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/flow-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/flow-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/trail.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/triangles-abstract.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/flower-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/watercolour.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/tiles-pattern.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/moire-pattern.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/tiles-illusion.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/tiles-illusion-2.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/points-3.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/points-4.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/points-5.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/points-6.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/canvas-playground/-/raw/master/screenshots/points-7.png" loading="lazy" width="290" height="410">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/athens-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/berlin-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/boston-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/london-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/new-york-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/rome-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/thessaloniki-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/tokyo-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/barcelona-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/beijing-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/cape-town-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/chicago-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/lisbon-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/paris-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/stockholm-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/madrid-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/moscow-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/copenhagen-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/redhook-milano-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/radrace-hamburg-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/transcontinental-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/oregon-cascades-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/arizona-trail-race-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/spartathlon-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/zagori-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/badwater-small.png" width="290">
        <img src="https://gitlab.com/pragalakis/gpx-visualizer/-/raw/master/posters/images/olympus-small.png" width="290">
      </div>
    </body>
  `;
};

const prefix = css`
  :host {
    background: #ff6e7f;
    background: -webkit-linear-gradient(to top, #bfe9ff, #ff6e7f);
    background: linear-gradient(to top, #bfe9ff, #ff6e7f);
  }

  :host img {
    display: inline-block;
  }

  :host > div {
    text-align: center;
    margin: 0 auto;
  }
`;

module.exports = showcase;
