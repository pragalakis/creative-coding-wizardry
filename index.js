const html = require('choo/html');
const choo = require('choo');
const css = require('sheetify');
const canvasSketch = require('canvas-sketch');
const notFound = require('./404.js');
const showcase = require('./showcase.js');
const playButton = require('./canvas/play-button.js');

const draw = {
  page_1: require('./canvas/position.js'),
  page_2: require('./canvas/sizes.js'),
  page_3: require('./canvas/line.js'),
  page_4: require('./canvas/rect.js'),
  page_5: require('./canvas/arc.js'),
  page_6: require('./canvas/curve.js'),
  page_7: require('./canvas/random.js'),
  page_8: require('./canvas/loop.js'),
  page_9: require('./canvas/image.js'),
  page_10: require('./canvas/generative-lines.js'),
  page_11: require('./canvas/text.js'),
  page_12: require('./canvas/squares-displacement.js'),
  page_13: require('./canvas/lines-displacement.js'),
  page_14: require('./canvas/half-arc.js'),
  page_15: require('./canvas/cubic-disarray.js'),
  page_16: require('./canvas/bloabs-stacked.js'),
  page_17: require('./canvas/noise.js')
};

const app = choo();
app.use(pagesStore);
app.route('/', mainView); // dev
app.route('/creative-coding-wizardry', mainView); // prod
app.route('/creative-coding-wizardry/showcase', showcase); // prod
app.route('/creative-coding-wizardry/404', notFound); // prod
app.route('/creative-coding-wizardry/*', notFound); // prod
app.mount('body');

function pagesStore(state, emitter) {
  state.activePage = 0;
  emitter.on('setActivePage', page => {
    state.activePage = page;
    emitter.emit('render');
  });
}

function mainView(state, emit) {
  emit('DOMTitleChange', 'Creative Coding Wizardry');

  const nextPage = () => {
    if (state.activePage < 17) {
      emit('setActivePage', state.activePage + 1);
    }
  };

  const prevPage = () => {
    if (state.activePage > 0) {
      emit('setActivePage', state.activePage - 1);
    }
  };

  document.body.onload = () => {
    const canvas = document.getElementById('canvas-play-button');
    playButton(canvas);
  };

  return html`
    <body class=${prefix}>
      <a href="/creative-coding-wizardry/showcase">
        <canvas id="canvas-play-button" width="60" height="60"></canvas>
      </a>
      <div class="buttons">
        <button onclick=${prevPage} class="navigation">
          <img src='./assets/minus.png' />
        </button>
        <button onclick=${nextPage} class="navigation">
          <img src='./assets/plus.png' />
        </button>
      </div>
      <img src='./assets/frame.png' className="frame" />
      ${
        state.activePage === 0
          ? html`<img src="./assets/intro.png" className="intro" />`
          : canvasSketch(draw[`page_${state.activePage}`], {
              dimensions: 'A3',
              pixelsPerInch: 72,
              units: 'cm'
            })
      }
    </body>
  `;
}

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-serif;
    display: flex;
    height: 100vh;
    background-color: #ffe5e5;
    font-size: 16px;
    margin: 0;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  :host h1 {
    color: #f77;
    font-size: 2.5rem;
  }

  :host a {
    position: absolute;
    right: 30px;
    top: 30px;
  }

  :host .frame {
    position: absolute;
    z-index: 1;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    left: 0;
    right: 0;
  }

  :host #canvas-play-button {
    width: initial !important;
    height: initial !important;
  }

  :host canvas,
  :host .intro {
    width: 398px !important;
    height: auto !important;
  }

  :host .buttons {
    position: absolute;
    top: 30px;
    margin: 0 auto;
    text-align: center;
  }

  :host .navigation {
    background: transparent;
    border: none;
    padding: 0;
    margin: 0;
    width: 10%;
    height: auto;
  }

  :host .navigation img {
    width: 80%;
    height: auto;
  }
`;
