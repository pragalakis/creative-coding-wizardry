const html = require('choo/html');
const css = require('sheetify');

const notFound = () => {
  const sign = () => (Math.random() > 0.5 ? -1 : 1);
  document.body.onload = () => {
    const canvas = document.getElementById('canvas-404');
    const context = canvas.getContext('2d');
    const width = canvas.width;
    const height = canvas.height;

    context.strokeStyle = 'black';
    context.lineWidth = 2;
    context.font = '150px Monospace, Consolas, monaco, courier';

    const text = '404';
    const textWidth = context.measureText(text).width;

    let n = 10;
    while (n > 0) {
      const randomX = sign() * Math.random() * 50;
      const randomY = sign() * Math.random() * 50;
      context.strokeText(
        text,
        randomX + width / 2 - textWidth / 2,
        randomY + height / 2 + 70
      );
      n--;
    }
  };

  return html`
    <body class=${prefix}>
      <a href="/creative-coding-wizardry">
        ⇦ Route not found. Navigate back.
      </a>
      <canvas id="canvas-404" width="400" height="400"></canvas>
    </body>
  `;
};

const prefix = css`
  :host {
    margin: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;
    background: #ff6e7f;
    background: -webkit-linear-gradient(to top, #bfe9ff, #ff6e7f);
    background: linear-gradient(to top, #bfe9ff, #ff6e7f);
    background-repeat: no-repeat;
  }

  :host a {
    font-size: 17px;
    font-weight: 700;
    color: black;
    position: absolute;
    top: 20px;
    left: 20px;
  }
`;

module.exports = notFound;
